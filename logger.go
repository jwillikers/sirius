package main

type logger interface {
	Print(v ...interface{})
	Fatal(v ...interface{})
}
