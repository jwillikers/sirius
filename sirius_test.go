package main

import (
	"context"
	"errors"
	"os"
	"testing"
	"time"

	"bitbucket.org/athrun22/neterr"
)

func TestHasArrayChanged(t *testing.T) {
	t.Run("same", func(t *testing.T) {
		a1 := [2]string{"simon", "garfunkel"}
		a2 := [2]string{"simon", "garfunkel"}
		c := hasArrayChanged(a1, a2)
		e := false
		if e != c {
			t.Errorf("the return value of hasArrayChanged should be %t not %t", e, c)
		}
	})
	t.Run("different", func(t *testing.T) {
		a1 := [2]string{"simon", "garfunkel"}
		a2 := [2]string{"art", "paul"}
		c := hasArrayChanged(a1, a2)
		e := true
		if e != c {
			t.Errorf("the return value of hasArrayChanged should be %t not %t", e, c)
		}
	})
}

func TestGetArgURL(t *testing.T) {
	t.Run("given", func(t *testing.T) {
		e := "www.google.com"
		os.Args = []string{"cmd", e}
		url, err := getArgURL()
		if nil != err {
			t.Errorf("the error '%v' should not have been returned", err)
		}
		if e != url {
			t.Errorf("the url should be '%s' not '%s'", e, url)
		}
	})
	t.Run("notGiven", func(t *testing.T) {
		os.Args = []string{"cmd"}
		_, err := getArgURL()
		if nil == err {
			t.Errorf("an error should be returned because no URL is given as an argument")
		}
	})
	t.Run("empty", func(t *testing.T) {
		os.Args = []string{"cmd", ""}
		_, err := getArgURL()
		if nil == err {
			t.Errorf("an error should be returned because the URL is an empty string")
		}
	})
}

func TestGetArgFileName(t *testing.T) {
	t.Run("given", func(t *testing.T) {
		e := "filename.csv"
		os.Args = []string{"cmd", "www.google.com", e}
		fileName, err := getArgFileName()
		if nil != err {
			t.Errorf("the error '%v' should not have been returned", err)
		}
		if e != fileName {
			t.Errorf("the fileName should be '%s' not '%s'", e, fileName)
		}
	})
	t.Run("notGiven", func(t *testing.T) {
		e := "results.csv"
		os.Args = []string{"cmd", "www.google.com"}
		fileName, err := getArgFileName()
		if nil != err {
			t.Errorf("the error '%v' should not have been returned", err)
		}
		if e != fileName {
			t.Errorf("the fileName should be '%s' not '%s'", e, fileName)
		}
	})
	t.Run("empty", func(t *testing.T) {
		os.Args = []string{"cmd", "www.google.com", ""}
		_, err := getArgFileName()
		if nil == err {
			t.Errorf("an error should be returned because the file name is an empty string")
		}
	})
}

type DummyWriter struct {
	records [][]string
}

func newDummyWriter() *DummyWriter {
	w := new(DummyWriter)
	w.records = make([][]string, 0, 10)
	return w
}

func (w *DummyWriter) Write(record []string) error {
	w.records = append(w.records, record)
	return nil
}

func (w *DummyWriter) Flush() {}

func (w *DummyWriter) Error() error {
	return nil
}

func TestInitCSV(t *testing.T) {
	header := []string{"title", "artist", "timestamp"}
	w := newDummyWriter()
	err := initCSV(w)
	if nil != err {
		t.Errorf("the error '%v' should not have been returned", err)
	}
	w.Flush()
	if nil != w.Error() {
		t.Fatalf("the error '%v' should not have been encountered when flushing the writer's buffer", err)
	}
	if nil == w.records {
		t.Error("there should be values in the csv file")
	}
	if 1 != len(w.records) {
		t.Errorf("the length of the records read in should be 1 not '%d' because the header should be the only record", len(w.records))
	}
	record := w.records[0]
	if len(header) != len(record) {
		t.Errorf("the length of the header should be '%d' not '%d'", len(header), len(record))
	}
	for i := range header {
		if header[i] != record[i] {
			t.Errorf("the header value at index %d of the slice should be '%s' not '%s'", i, header[i], record[i])
		}
	}
}

func TestCloseWriter(t *testing.T) {
	w := newDummyWriter()
	err := closeWriter(w)
	if nil != err {
		t.Errorf("the error '%v' should not have been returned", err)
	}
}

type dummyScanner struct {
	scans [][]string
	errs  []error
	index int
}

const ScannerFinished = "nothing left to scan"

func newDummyScanner(scans [][]string, errs []error) *dummyScanner {
	s := new(dummyScanner)
	s.scans = scans
	s.errs = errs
	return s
}

func (s *dummyScanner) Scan(ctx context.Context, url string) (interface{}, error) {
	if s.index >= len(s.scans) || s.index >= len(s.errs) {
		return nil, neterr.New(ScannerFinished)
	}
	scanned := s.scans[s.index]
	err := s.errs[s.index]
	s.index++
	return scanned, err
}

type dummyScannerBadType struct{}

func (s *dummyScannerBadType) Scan(ctx context.Context, url string) (interface{}, error) {
	return 1, nil
}

type tmpError struct {
	err error
}

func newTmpError(err error) *tmpError {
	return &tmpError{err}
}

func (e *tmpError) Error() string {
	return e.err.Error()
}

func (e *tmpError) Temporary() bool {
	return true
}

type dummyWriterError struct{}

func (w *dummyWriterError) Write(record []string) error {
	return errors.New("this is a writer error")
}

func (w *dummyWriterError) Flush() {}

func (w *dummyWriterError) Error() error {
	return errors.New("this is a writer error")
}

func TestRecordSongs(t *testing.T) {
	scans := [][]string{
		[]string{"Witchcraft", "Bill Evans", "2018-07-11 08:04:30"},
		[]string{"Someday My Prince Will Come", "Bill Evans", "2018-07-11 08:06:30"},
		[]string{"Someday My Prince Will Come", "Miles Davis", "2018-07-11 08:10:59"},
		[]string{"Blue", "Joni Mitchell", "2018-07-11 08:15:00"},
		[]string{"Air For Free", "Relient K", "2018-07-11 08:17:14"},
	}
	t.Run("main", func(t *testing.T) {
		ctx, cancel := context.WithTimeout(context.Background(), 15*time.Millisecond)
		errs := []error{
			nil,
			nil,
			nil,
			nil,
			nil,
		}
		scanner := newDummyScanner(scans, errs)
		writer := newDummyWriter()
		errChan := make(chan error, 1)
		recordSongs(ctx, scanner, writer, "www.google.com", errChan, time.Nanosecond)
		cancel()
		err := <-errChan
		if nil == err {
			t.Errorf("expected error '%s' not nil", ScannerFinished)
		}
		if ScannerFinished != err.Error() {
			t.Errorf("expected error '%s' not '%v'", ScannerFinished, err)
		}
		if len(scanner.scans) != len(writer.records) {
			t.Errorf("expected the number of scanned songs %d to be the same as the number of written records %d", len(scanner.scans), len(writer.records))
		}
		for i := range scanner.scans {
			if len(scanner.scans[i]) != len(writer.records[i]) {
				t.Errorf("expected the length of the scanned song %s to be the same as the length of the written record %s", scanner.scans[i], writer.records[i])
			}
			for j := range scanner.scans[i] {
				if scanner.scans[i][j] != writer.records[i][j] {
					t.Errorf("expected the scanned song's string '%s' to be the same as written record's string '%s'", scanner.scans[i][j], writer.records[i][j])
				}
			}
		}
	})
	t.Run("context=done", func(t *testing.T) {
		ctx, cancel := context.WithCancel(context.Background())
		scanner := newDummyScanner(nil, nil)
		writer := newDummyWriter()
		errChan := make(chan error, 1)
		cancel()
		recordSongs(ctx, scanner, writer, "www.google.com", errChan, time.Nanosecond)
		err := <-errChan
		if nil != err {
			t.Errorf("expected error to be nil not '%v'", err)
		}
	})
	t.Run("error=scanner=temporary", func(t *testing.T) {
		ctx, cancel := context.WithTimeout(context.Background(), 15*time.Millisecond)
		errs := []error{
			nil,
			nil,
			newTmpError(errors.New("this is a temporary error")),
			nil,
			nil,
			nil,
		}
		scanner := newDummyScanner(scans, errs)
		writer := newDummyWriter()
		errChan := make(chan error, 1)
		recordSongs(ctx, scanner, writer, "www.google.com", errChan, time.Nanosecond)
		cancel()
		err := <-errChan
		if nil == err {
			t.Errorf("expected error '%s' not nil", ScannerFinished)
		}
		if ScannerFinished != err.Error() {
			t.Errorf("expected error '%s' not '%v'", ScannerFinished, err)
		}
	})
	t.Run("error=scanner=badType", func(t *testing.T) {
		ctx, cancel := context.WithCancel(context.Background())
		scanner := new(dummyScannerBadType)
		writer := newDummyWriter()
		errChan := make(chan error, 1)
		recordSongs(ctx, scanner, writer, "www.google.com", errChan, time.Nanosecond)
		cancel()
		err := <-errChan
		if nil == err {
			t.Errorf("expected fatal error not nil")
		}
		tmp := neterr.Wrap(err)
		if tmp.Temporary() {
			t.Errorf("expected a fatal error not a temporary error '%v'", err)
		}
	})
	t.Run("error=writer", func(t *testing.T) {
		ctx, cancel := context.WithCancel(context.Background())
		errs := []error{
			nil,
		}
		scanner := newDummyScanner(scans, errs)
		writer := new(dummyWriterError)
		errChan := make(chan error, 1)
		recordSongs(ctx, scanner, writer, "www.google.com", errChan, time.Nanosecond)
		cancel()
		err := <-errChan
		if nil == err {
			t.Error("expected a fatal error not nil")
		}
		tmp := neterr.Wrap(err)
		if tmp.Temporary() {
			t.Errorf("expected a fatal error not a temporary error '%v'", err)
		}
	})
}

type mockLogger struct {
	msg   string
	fatal bool
}

func (l *mockLogger) Print(v ...interface{}) {
	switch i := v[0].(type) {
	case error:
		l.msg = i.Error()
	case string:
		l.msg = i
	}
}

func (l *mockLogger) Fatal(v ...interface{}) {
	switch i := v[0].(type) {
	case error:
		l.msg = i.Error()
	case string:
		l.msg = i
	}
	l.fatal = true
}

func TestLogErr(t *testing.T) {
	t.Run("error=nil", func(t *testing.T) {
		l := new(mockLogger)
		logErr(l, nil)
		if "" != l.msg {
			t.Error("the logger's message should not have been modified because there was no error")
		}
		if l.fatal {
			t.Error("the logger's fatal status should not have been modified because there was no error")
		}
	})
	t.Run("error=temporary", func(t *testing.T) {
		l := new(mockLogger)
		msg := "this is a temporary error"
		logErr(l, newTmpError(errors.New(msg)))
		if msg != l.msg {
			t.Errorf("the logger's message should be '%s' not '%s'", msg, l.msg)
		}
		if l.fatal {
			t.Errorf("the logger's fatal status should be false not %t", l.fatal)
		}
	})
	t.Run("error=fatal", func(t *testing.T) {
		l := new(mockLogger)
		msg := "this is a fatal error"
		logErr(l, neterr.New(msg))
		if msg != l.msg {
			t.Errorf("the logger's message should be '%s' not '%s'", msg, l.msg)
		}
		if !l.fatal {
			t.Errorf("the logger's fatal status should be true not %t", l.fatal)
		}
	})
}
