package main

type writer interface {
	Write(s []string) error
	Flush()
	Error() error
}
