package main

import (
	"context"
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"reflect"
	"time"

	"bitbucket.org/athrun22/neterr"
	"bitbucket.org/athrun22/pulpg"
	"bitbucket.org/athrun22/scrapegoat"
	"bitbucket.org/athrun22/sirius/siriusweb"
	"bitbucket.org/athrun22/sitechg"
)

func hasArrayChanged(x, y interface{}) bool {
	changed := false
	s1, ok := x.([2]string)
	if ok {
		s2, ok := y.([2]string)
		if ok {
			if s1 != s2 {
				changed = true
			}
		}
	}
	return changed
}

func getArgURL() (string, error) {
	var url string
	var err error
	if len(os.Args) > 1 {
		if os.Args[1] == "" {
			err = neterr.New("the siriusxm channel's url cannot be an empty string")
		} else {
			url = os.Args[1]
		}
	} else {
		err = neterr.New("please supply the url of the channel when calling the sirius executable")
	}
	return url, err
}

func getArgFileName() (string, error) {
	var filename string
	var err error
	if len(os.Args) > 2 {
		if os.Args[2] == "" {
			err = neterr.New("the file's name cannot be an empty string")
		} else {
			filename = os.Args[2]
		}
	} else {
		filename = "results.csv"
	}
	return filename, err
}

func initCSV(w writer) error {
	header := []string{"title", "artist", "timestamp"}
	return w.Write(header)
}

func closeWriter(w writer) error {
	w.Flush()
	return w.Error()
}

func recordSongs(ctx context.Context, sca sitechg.Scanner, w writer, url string, errCh chan<- error, tempErrWait time.Duration) {
	var err error
	breakLoop := false
	timeout := time.After(0)
	for {
		select {
		case <-ctx.Done():
			breakLoop = true
			break
		case <-timeout:
			scanCtx, scanCancel := context.WithCancel(ctx)
			var next interface{}
			next, err = sca.Scan(scanCtx, url)
			scanCancel()
			if err != nil {
				netErr := neterr.Wrap(err)
				if !netErr.Temporary() {
					err = netErr
					breakLoop = true
					break
				}
			} else {
				s, ok := next.([]string)
				if !ok {
					err = neterr.New(fmt.Sprintf("the scanned object should be a []string not a %s", reflect.TypeOf(next)))
					breakLoop = true
					break
				}
				err = w.Write(s)
				if err != nil {
					err = neterr.New("error writing song to csv file")
					breakLoop = true
					break
				}
			}
			timeout = time.After(tempErrWait)
		}
		if breakLoop {
			break
		}
	}
	errCh <- err
}

func logErr(l logger, err error) {
	if err == nil {
		return
	}
	netErr := neterr.Wrap(err)
	if netErr.Temporary() {
		l.Print(err)
	} else {
		l.Fatal(err)
	}
}

func main() {
	logger := log.New(os.Stdout, "Sirius: ", log.LstdFlags)

	url, err := getArgURL()
	logErr(logger, err)

	filename, err := getArgFileName()
	logErr(logger, err)

	logger.Print("starting up chromedp instance")
	chrome, err := pulpg.NewCdp(context.Background())
	logErr(logger, err)

	pul := pulpg.NewCdpPuller(chrome, `#onair-pdt`)

	logger.Printf("creating file '%s'", filename)
	f, err := os.Create(filename)
	logErr(logger, err)
	defer f.Close()

	w := csv.NewWriter(f)

	logger.Print("initializing csv file")
	err = initCSV(w)
	logErr(logger, err)

	scr := scrapegoat.NewPageScraper(pul, siriusweb.NewSongParser(new(siriusweb.TimeNower)))
	logErr(logger, err)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	logger.Print("starting go routine to handle signals")
	caught := make(chan os.Signal, 1)
	defer close(caught)

	go handleSignals(caught)

	sca := sitechg.NewDynamicScanner(scr, 60*time.Second, hasArrayChanged)

	errCh := make(chan error)
	defer close(errCh)

	logger.Print("starting go routine to record new songs")
	go recordSongs(ctx, sca, w, url, errCh, 15*time.Second)

	select {
	case err = <-errCh:
		logErr(logger, err)
	case sig := <-caught:
		logger.Printf("caught signal '%v'", sig)
		cancel()
	}

	logger.Print("writing out the buffered output to the csv")
	err = closeWriter(w)
	logErr(logger, err)

	ctxShutdown, cancelShutdown := context.WithTimeout(context.Background(), 60*time.Second)
	defer cancelShutdown()

	logger.Print("shutting down the CdpScraper")
	err = pul.Shutdown(ctxShutdown)
	logErr(logger, err)
}
