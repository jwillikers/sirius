package main

import (
	"os"
	"os/signal"
	"syscall"
)

func handleSignals(caught chan os.Signal) {
	signal.Ignore(syscall.SIGCHLD, syscall.SIGWINCH)
	signal.Notify(caught)
	s := <-caught
	signal.Reset(s)
}
