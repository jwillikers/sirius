Sirius
===
Sirius catalogues songs played on a SiriusXM Radio channel.

Getting Started
---
The following instructions detail how to use the program.

### Prerequisites
-   [go programming language](https://golang.org/doc/install)
-   [chrome web browser](https://www.google.com/chrome/)

### Installing
Retrieve the source files of the program.
```console
go get bitbucket.org/athrun22/sirius
```

### Running
To begin cataloging songs, run the sirius script, giving it the SiriusXM channel's url. It will write songs out to the file results.csv by default.
```console
$GOPATH/bin/sirius "https://www.siriusxm.com/classicrewind"
```
A second argument can be supplied to write the output to that file.
```console
$GOPATH/bin/sirius "https://www.siriusxm.com/classicrewind" "songs.csv"
```

### Updating
To stay up-to-date, occasionally use go get to retrieve the latest source by running the go get command above or running go's package update command that follows.
```console
go get -u
```

Built With
---
-   [chromedp](https://github.com/chromedp/chromedp) - a faster, simpler way to drive browsers without external dependencies

Versioning
---
[SemVer](http://semver.org/) is used for versioning. For the versions available, see the tags on this repository.

Contributing
---
Please read [CONTRIBUTING.md](./CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

Authors
---
-   Jordan Williams

License
---
This project is licensed under the MIT License - see the [LICENSE.md](./LICENSE.md) file for details
