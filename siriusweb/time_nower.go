package siriusweb

import "time"

// TimeNower calls the time.Now() function.
type TimeNower struct{}

// Now returns the time it is at the instant of its calling.
func (n *TimeNower) Now() time.Time {
	return time.Now()
}
