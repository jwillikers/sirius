package siriusweb

import (
	"testing"
	"time"

	"bitbucket.org/athrun22/neterr"
)

type mockNower struct {
	t time.Time
}

func newMockNower(t time.Time) *mockNower {
	return &mockNower{t}
}

func (n *mockNower) Now() time.Time {
	return n.t
}

func TestParse(t *testing.T) {
	n := newMockNower(time.Date(2018, time.July, 11, 8, 4, 30, 15, time.UTC))
	sp := NewSongParser(n)
	t.Run("song=1", func(t *testing.T) {
		html := `

									<img src="//www.siriusxm.com/albumart/Live/2000/patbenatar_58E67E7F_t.jpg" alt="">
									<p class="onair-pdt-artist">Pat Benatar</p><p class="onair-pdt-song">Promises In The Dark</p>`
		e := [3]string{"Promises In The Dark", "Pat Benatar", "2018-07-11 08:04:30"}
		i, err := sp.Parse(&html)
		if nil != err {
			t.Errorf("the error '%v' should not be returned", err)
		}
		if nil == i {
			t.Error("the interface should not be nil")
		}
		s, ok := i.([3]string)
		if !ok {
			t.Error("the interface should be a string array of length 3")
		}
		if len(e) != len(s) {
			t.Errorf("the length of the array should be %d not %d", len(e), len(s))
		}
		if e[0] != s[0] {
			t.Errorf("the first string should be '%s' not '%s'", e[0], s[0])
		}
		if e[1] != s[1] {
			t.Errorf("the second string should be '%s' not '%s'", e[1], s[1])
		}
		if e[2] != s[2] {
			t.Errorf("the third string should be '%s' not '%s'", e[2], s[2])
		}
	})
	t.Run("song=2", func(t *testing.T) {
		html := `

									<img src="//www.siriusxm.com/albumart/Live/2030/eddiemoney_56753E91_t.jpg" alt="">
									<p class="onair-pdt-artist">Eddie Money</p><p class="onair-pdt-song">Take Me Home Tonight</p>`
		e := [3]string{"Take Me Home Tonight", "Eddie Money", "2018-07-11 08:04:30"}
		i, err := sp.Parse(&html)
		if nil != err {
			t.Errorf("the error '%v' should not be returned", err)
		}
		if nil == i {
			t.Error("the interface should not be nil")
		}
		s, ok := i.([3]string)
		if !ok {
			t.Error("the interface should be a string array of length 3")
		}
		if len(e) != len(s) {
			t.Errorf("the length of the array should be %d not %d", len(e), len(s))
		}
		if e[0] != s[0] {
			t.Errorf("the first string should be '%s' not '%s'", e[0], s[0])
		}
		if e[1] != s[1] {
			t.Errorf("the second string should be '%s' not '%s'", e[1], s[1])
		}
		if e[2] != s[2] {
			t.Errorf("the third string should be '%s' not '%s'", e[2], s[2])
		}
	})
	t.Run("song=3", func(t *testing.T) {
		html := `

									<img src="//www.siriusxm.com/albumart/Live/2100/kansas_4ECAEB5D_t.jpg" alt="">
									<p class="onair-pdt-artist">Kansas</p><p class="onair-pdt-song">Point Of Know Return</p>`
		e := [3]string{"Point Of Know Return", "Kansas", "2018-07-11 08:04:30"}
		i, err := sp.Parse(&html)
		if nil != err {
			t.Errorf("the error '%v' should not be returned", err)
		}
		if nil == i {
			t.Error("the interface should not be nil")
		}
		s, ok := i.([3]string)
		if !ok {
			t.Error("the interface should be a string array of length 3")
		}
		if len(e) != len(s) {
			t.Errorf("the length of the slice should be %d not %d", len(e), len(s))
		}
		if e[0] != s[0] {
			t.Errorf("the first string should be '%s' not '%s'", e[0], s[0])
		}
		if e[1] != s[1] {
			t.Errorf("the second string should be '%s' not '%s'", e[1], s[1])
		}
		if e[2] != s[2] {
			t.Errorf("the third string should be '%s' not '%s'", e[2], s[2])
		}
	})
	t.Run("invalidArtist", func(t *testing.T) {
		html := "<p class=\"onair-pdt-artist\">@artist</p>"
		_, err := sp.Parse(&html)
		if nil == err {
			t.Errorf("an error should be returned")
		}
		_, ok := err.(*neterr.NetError)
		if !ok {
			t.Errorf("the error '%v' should be of type NetError", err)
		}
		e := "the song's artist '@artist' is invalid"
		if e != err.Error() {
			t.Errorf("the error message should be '%s' not '%s'", e, err.Error())
		}
	})
	t.Run("invalidTitle", func(t *testing.T) {
		html := "<p class=\"onair-pdt-song\">Ch. 103</p>"
		_, err := sp.Parse(&html)
		if nil == err {
			t.Errorf("an error should be returned")
		}
		_, ok := err.(*neterr.NetError)
		if !ok {
			t.Errorf("the error '%v' should be of type NetError", err)
		}
		e := "the song's title 'Ch. 103' is invalid"
		if e != err.Error() {
			t.Errorf("the error message should be '%s' not '%s'", e, err.Error())
		}
	})
	t.Run("emptyHtml", func(t *testing.T) {
		html := ""
		_, err := sp.Parse(&html)
		if nil == err {
			t.Errorf("an error should be returned")
		}
		_, ok := err.(*neterr.NetError)
		if !ok {
			t.Errorf("the error '%v' should be of type ParsingError", err)
		}
		e := "reached the end of the html without finding the artist and title of the song"
		if e != err.Error() {
			t.Errorf("the error message should be '%s' not '%s'", e, err.Error())
		}
	})
	t.Run("ValidTitleNoArtist", func(t *testing.T) {
		html := "<\\br><p class=\"onair-pdt-song\">No Control</p></br>"
		_, err := sp.Parse(&html)
		if nil == err {
			t.Errorf("an error should be returned")
		}
		_, ok := err.(*neterr.NetError)
		if !ok {
			t.Errorf("the error '%v' should be of type NetError", err)
		}
		e := "reached the end of the html without finding the artist and title of the song"
		if e != err.Error() {
			t.Errorf("the error message should be '%s' not '%s'", e, err.Error())
		}
	})
	t.Run("ValidArtistNoTitle", func(t *testing.T) {
		html := "<\\br><p class=\"onair-pdt-artist\">Brand New</p></br>"
		_, err := sp.Parse(&html)
		if nil == err {
			t.Errorf("an error should be returned")
		}
		_, ok := err.(*neterr.NetError)
		if !ok {
			t.Errorf("the error '%v' should be of type NetError", err)
		}
		e := "reached the end of the html without finding the artist and title of the song"
		if e != err.Error() {
			t.Errorf("the error message should be '%s' not '%s'", e, err.Error())
		}
	})
}

func BenchmarkParseSong(b *testing.B) {
	sp := NewSongParser(newMockNower(time.Now()))
	b.Run("song=1", func(b *testing.B) {
		html := `

									<img src="//www.siriusxm.com/albumart/Live/2000/patbenatar_58E67E7F_t.jpg" alt="">
									<p class="onair-pdt-artist">Pat Benatar</p><p class="onair-pdt-song">Promises In The Dark</p>`
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			sp.Parse(&html)
		}
	})
	b.Run("song=2", func(b *testing.B) {
		html := `

									<img src="//www.siriusxm.com/albumart/Live/2030/eddiemoney_56753E91_t.jpg" alt="">
									<p class="onair-pdt-artist">Eddie Money</p><p class="onair-pdt-song">Take Me Home Tonight</p>`
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			sp.Parse(&html)
		}
	})
	b.Run("song=3", func(b *testing.B) {
		html := `

									<img src="//www.siriusxm.com/albumart/Live/2100/kansas_4ECAEB5D_t.jpg" alt="">
									<p class="onair-pdt-artist">Kansas</p><p class="onair-pdt-song">Point Of Know Return</p>`
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			sp.Parse(&html)
		}
	})
}

func TestIsValidTitle(t *testing.T) {
	t.Run("title=valid", func(t *testing.T) {
		title := "No Control"
		if !isValidTitle(&title) {
			t.Errorf("the title '%s' should be valid", title)
		}
	})
	t.Run("title=empty", func(t *testing.T) {
		title := ""
		if isValidTitle(&title) {
			t.Error("an empty title should not be valid")
		}
	})
	t.Run("title=ad1", func(t *testing.T) {
		title := "fb.com/ad"
		if isValidTitle(&title) {
			t.Errorf("the title '%s' should not be valid", title)
		}
	})
	t.Run("title=ad2", func(t *testing.T) {
		title := "Ch103"
		if isValidTitle(&title) {
			t.Errorf("the title '%s' should not be valid", title)
		}
	})
	t.Run("title=ad3", func(t *testing.T) {
		title := "Ch. 103"
		if isValidTitle(&title) {
			t.Errorf("the title '%s' should not be valid", title)
		}
	})
}

func TestIsValidArtist(t *testing.T) {
	t.Run("artist=valid", func(t *testing.T) {
		artist := "Brand New"
		if !isValidArtist(&artist) {
			t.Errorf("the artist '%s' should be valid", artist)
		}
	})
	t.Run("artist=empty", func(t *testing.T) {
		artist := ""
		if isValidArtist(&artist) {
			t.Error("an empty artist should not be valid")
		}
	})
	t.Run("artist=ad", func(t *testing.T) {
		artist := "@artist"
		if isValidArtist(&artist) {
			t.Errorf("the artist '%s' should not be valid", artist)
		}
	})
}
