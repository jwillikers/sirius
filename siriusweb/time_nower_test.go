package siriusweb

import (
	"testing"
	"time"
)

func TestTimeNower(t *testing.T) {
	n := new(TimeNower)
	e := time.Now().Format("2006-01-02 15:04:05")
	now := n.Now().Format("2006-01-02 15:04:05")
	if e != now {
		t.Errorf("time should be '%s' not '%s'", e, now)
	}
}
