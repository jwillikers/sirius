package siriusweb

import (
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/athrun22/neterr"
	"golang.org/x/net/html"
)

// SongParser parses song titles and artists.
type SongParser struct {
	time nower
}

//NewSongParser takes in a "nower" object for calling Now() on for time reference.
func NewSongParser(time nower) *SongParser {
	return &SongParser{time}
}

func isValidTitle(title *string) bool {
	adPrefixes := []string{
		"fb.com/",
		"Ch.",
	}

	isValid := *title != ""

	for _, prefix := range adPrefixes {
		if !isValid {
			break
		} else if strings.HasPrefix(*title, prefix) {
			isValid = false
		}
	}

	// name has advertisement name like Ch103
	if isValid && strings.HasPrefix(*title, "Ch") {
		if _, conversionError := strconv.Atoi((*title)[2:]); conversionError == nil {
			isValid = false
		}
	}

	return isValid
}

func isValidArtist(artist *string) bool {
	isValid := true
	if *artist == "" {
		isValid = false
	} else if strings.HasPrefix(*artist, "@") {
		isValid = false
	}
	return isValid
}

// Parse retrieves the SiriusXM song data from the OnAir portion of the HTML of a channel's webpage
func (p *SongParser) Parse(onAirHTML *string) (interface{}, error) {
	var err error
	var song [3]string
	previousStartToken := ""
	foundArtist := false
	foundTitle := false
	tokenizer := html.NewTokenizerFragment(strings.NewReader(*onAirHTML), "div")
	breakLoop := false

	for {
		token := tokenizer.Next()

		switch {
		case token == html.ErrorToken:
			err = neterr.NewTemporary("reached the end of the html without finding the artist and title of the song")
			breakLoop = true
			break

		case token == html.TextToken && previousStartToken != "":
			text := strings.TrimSpace(html.UnescapeString(string(tokenizer.Text())))
			if previousStartToken == "artist" {
				if isValidArtist(&text) {
					song[1] = text
					foundArtist = true
				} else {
					err = neterr.New(fmt.Sprintf("the song's artist '%s' is invalid", text))
					breakLoop = true
					break
				}
			} else if previousStartToken == "title" {
				if isValidTitle(&text) {
					song[0] = text
					foundTitle = true
				} else {
					err = neterr.New(fmt.Sprintf("the song's title '%s' is invalid", text))
					breakLoop = true
					break
				}
			}
			if foundArtist == true && foundTitle == true {
				song[2] = p.time.Now().Format("2006-01-02 15:04:05")
				breakLoop = true
				break
			} else {
				previousStartToken = ""
			}

		case token == html.StartTagToken:
			tag := tokenizer.Token()

			if tag.DataAtom == 0xc01 {
				for _, p := range tag.Attr {
					if p.Key == "class" {
						if p.Val == "onair-pdt-artist" {
							previousStartToken = "artist"
							break
						} else if p.Val == "onair-pdt-song" {
							previousStartToken = "title"
							break
						}
					}
				}
			}
		}
		if breakLoop {
			break
		}
	}
	return song[:], err
}
